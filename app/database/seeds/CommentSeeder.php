<?php

class CommentSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('comments');
		$db->delete();

		$comments = [
			[
				'id'		=>	1,
				'parent_id'	=>	0,
				'body'		=>	'Lorem ipsum ang gwapo ko talaga!',
				'name'		=>	'srph',
				'email'		=>	'srph@srph.com'
			],

			[
				'id'		=>	2,
				'parent_id'	=>	1,
				'body'		=>	'Oh talaga tol? Dapat sinapak mo!',
				'name'		=>	'srph',
				'email'		=>	'srph@srph.com'
			],


			[
				'id'		=>	3,
				'parent_id'	=>	1,
				'body'		=>	'Oh talaga tol? Dapat sinapak mo!',
				'name'		=>	'Anon',
				'email'		=>	'srph@srph.com'
			],

			[
				'id'		=>	4,
				'parent_id'	=>	0,
				'body'		=>	'Lorem ipsum ang gwapo ko talaga!',
				'name'		=>	'srph',
				'email'		=>	'srph@srph.com'
			],

			[
				'id'		=>	5,
				'parent_id'	=>	4,
				'body'		=>	'Oh talaga tol? Dapat sinapak mo!',
				'name'		=>	'Anonymous',
				'email'		=>	'srph@srph.com'
			],

			[
				'id'		=>	6,
				'parent_id'	=>	0,
				'body'		=>	'Lorem ipsum ang gwapo ko talaga!',
				'name'		=>	'srph',
				'email'		=>	'srph@srph.com'
			]
		];

		$db->insert($comments);
	}

}