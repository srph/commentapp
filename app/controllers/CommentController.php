<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(array(
			'comments' => Comment::get()->toArray()
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$name = (Input::has('name'))
			? Input::get('name')
			: 'Anonymous';

		$comment = new Comment(array(
			'parent_id'	=>	0,
			'name'		=>	$name,
			'body'		=>	Input::get('body'),
			'email'		=>	Input::get('email')
		));

		if($comment->save()) {
			return Response::json(array('status' => true));
		}

		return Response::json(array('status' => false));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$comment = Comment::find($id)->toArray();
		return Response::json(array('comment' => $comment));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$comment = Comment::find($id);
		$status = ($comment->delete())
			? true
			: false;

		return Response::json(array('status' => $status));
	}

}