<!DOCTYPE html>
<html ng-app="commentApp">
<head>
	<title> Comments </title>
	<meta charset="utf-8">
	<base href="/">
	<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="assets/css/stylesheet.css">
	<link type="text/css" rel="stylesheet" href="assets/css/loading-bar.css">
</head>

<body>
	<div class="container">
		<!-- Navigation -->
		<div class="navbar navbar-default"
			role="navigation">
			<div class="collapse navbar-collapse"
				id="main-navigation">
				<ul class="nav navbar-nav">
					<li><a href="#"> Home </a></li>
				</ul>
			</div>
		</div>

		<div ng-view></div>
	</div>

	<!-- Scripts -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/angular.min.js"></script>
	<script src="assets/js/angular-route.min.js"></script>
	<script src="assets/js/loading-bar.js"></script>
	<script src="assets/js/angular-animate.min.js"></script>
	<script src="app/main.js"></script>
	<script src="app/controllers/CommentCtrl.js"></script>
	<script src="app/controllers/RequestCtrl.js"></script>
	<!-- <script src="app/directives/"></script> -->
	<script src="app/services/Comment.js"></script>
</body>
</html>
