var app = angular.module('commentApp', ['ngRoute', 'chieffancypants.loadingBar', 'ngAnimate']);

app.config(function($routeProvider, $locationProvider, cfpLoadingBarProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'app/templates/list.html',
			controller: 'CommentCtrl'
		})
		.when('/:id', {
			templateUrl: 'app/templates/comment.html',
			controller: 'RequestCtrl'
		});

	$locationProvider.html5Mode(true);
	cfpLoadingBarProvider.includeSpinner = false;
});