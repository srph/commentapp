app.factory('Comment', function($http) {
	return {
		all: function() {
			return $http.get('api/comment/');
		},

		get: function(id) {
			return $http.get('api/comment/' + id);
		},

		save : function(commentData) {
			return $http({
				method: 'POST',
				url: '/api/comment',
				headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
				data: $.param(commentData)
			});
		},

		destroy: function(id) {
			return $http.delete('api/comment/' + id);
		}
	}
});