app.controller('RequestCtrl', function($scope, $http, $routeParams, $location, Comment) {
	$scope.comment = {};
	Comment.get($routeParams.id)
		.success(function(data) {
			$scope.comment = data.comment;
		})

	$scope.delete = function(id) {

		Comment.destroy(id)
			.success(function(data) {
				$location.path('/');
			})
	}
})