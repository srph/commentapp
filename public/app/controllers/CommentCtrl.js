app.controller('CommentCtrl', function($scope, $http, Comment) {
	$scope.commentData = {};
	$scope.comments = {};

	// Loading variable to show the loading bar
	$scope.loading = true;

	Comment.all()
		.success(function(data) {
			$scope.comments = data.comments;
			$scope.loading = false;
		});

	$scope.submit = function() {
		$scope.loading = true;

		Comment.save($scope.commentData)
			.success(function(data) {
				console.log(data);
				$scope.loading = false;
				if(data.status) {
					alert('wow');
				}
				Comment.all()
					.success(function(data) {
						$scope.comments = data.comments;
						$scope.loading = false;
					});
			})
			.error(function(data) {
				console.log('hay');
			});
	}

	$scope.delete = function(id) {
		$scope.loading = true;

		Comment.destroy(id)
			.success(function(data) {
				
				Comment.all()
					.success(function(data) {
						$scope.comments = data.comments;
						$scope.loading = false;
					});
				
			})
	}
})